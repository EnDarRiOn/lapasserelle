<?php get_header();?>

	<section id="primary-section" class="content-primary clearfix">

		<?php if( is_post_type_archive( 'destinations' ) ):

		get_template_part( 'template-page/tpl-page', 'destination' );

		elseif( is_post_type_archive( 'agenda' ) ):

		get_template_part( 'template-page/tpl-page', 'agenda' );

		elseif( is_post_type_archive( 'lieux' ) ):

		get_template_part( 'template-page/tpl-page', 'destination' );

		else :

		get_template_part( 'template-page/tpl-page', 'content' );

		endif;?>




<?php get_footer();?>