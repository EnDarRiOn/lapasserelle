<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<section id="primary-section" class="content-primary container-primary-page">
		<div class="container">
			<header class="entry-header-single container-header clearfix container">
					<h1 class="title-page title-single">
						Résultats de votre recherche
					</h1>
					<h2><?php printf( __( 'Pour le ou les mot(s) : %s' ), '<span class="words-resluts">' . esc_html( get_search_query() ) . '</span>' ); ?></h2>
			</header>
		</div>
	</section>
	<section id="secondary-section" class="content-secondary container-page clearfix results found">

			<div class="container">
				<div class="content-single container-column content-search clearfix">
					<ul>
						<?php while ( have_posts() ) : the_post();?>
							<?php get_template_part( 'template-part/content', 'search' );?>
						<?php endwhile;?>
					</ul>
				</div>
			</div>

	</section>
<?php else :?>
	<section id="primary-section" class="content-primary container-primary-page">
		<div class="container">
			<header class="entry-header-single container-header clearfix container">
				<h1 class="title-page title-single"><?php _e( 'Nothing Found', 'twentysixteen' ); ?></h1>
			</header>
		</div>
	</section>
	<section id="secondary-section" class="content-secondary container-page clearfix no-results not-found">
		<div class="container">
			<div class="content-single container-column content-search clearfix">
				<?php get_template_part( 'template-part/content', 'none' );?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>