<?php get_header();

	if( is_singular( 'destinations' ) ):

		get_template_part( 'template-single/tpl-single', 'destinations' );

	elseif( is_singular( 'lieux' ) ):

		get_template_part( 'template-single/tpl-single', 'lieux' );

	elseif( is_singular( 'emissions' ) ):

		get_template_part( 'template-single/tpl-single', 'emissions' );

	elseif( is_singular( 'partenaires' ) ):

		get_template_part( 'template-single/tpl-single', 'partenaires' );

	elseif( is_singular( 'agenda' ) ):

		get_template_part( 'template-single/tpl-single', 'agenda' );

	else:

	get_template_part( 'template-single/tpl', 'single' );

	endif;

get_footer();?>