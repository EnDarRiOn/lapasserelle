<?php
//IMAGE À LA UNE
add_theme_support( 'post-thumbnails' );

//IMAGES RESPONSIVE
function responsive_images( $html ) {
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return $html;
}
add_filter( 'post_thumbnail_html', 'responsive_images', 10 );
add_filter( 'image_send_to_editor', 'responsive_images', 10 );
add_filter( 'wp_get_attachment_link', 'responsive_images', 10 );

add_filter('wp_calculate_image_srcset_meta', '__return_null');

//NOUVELLE TAILLE D'IMAGE
function new_format_image(){
	add_image_size( 'img-top', 2540, 1430, true );
	add_image_size( '12col', 2425, 880, true );
	add_image_size( '10col', 2000, 865, true );
	add_image_size( '8col', 1350, 865, true );
	add_image_size( '8col-demi', 1350, 505, true );
	add_image_size( '8col-tiers', 1350, 535, true );
	add_image_size( '6col', 1188, 810, true );
	add_image_size( '4col', 774, 506, true );
	add_image_size( '4col-work', 774, 774, true );
	add_image_size( 'portrait', 2540, 1150, true );
}
add_action( 'after_setup_theme', 'new_format_image' );