<?php get_header(); ?>

		<section id="primary" class="content-primary container">

			<section class="widget-container">


			<?php
					get_template_part( 'template-part/content', 'galerie' );

			?>

			<?php
					get_template_part( 'template-part/content', 'last-sound' );

			?>


			</section>

		</section><!-- .content-primary -->

		<section id="secondary" class="content-secondary"></section><!-- .content-secondary -->

		<section id="tertiary" class="content-tertiary">

			<section class="about-container container clearfix">

				<article class="about-article">

					<h2 class="about-title about-block widget-block">À<br/>propos</h2>

					<?php

						if(have_posts()) : while(have_posts()) : the_post();


						the_content();

						endwhile; endif; wp_reset_query();

					?>

				</article>

			</section>

			<section class="clients-container container clearfix">

					<?php

						get_template_part( 'template-part/content', 'clients' );
					?>

			</section>

		</section><!-- .content-tertiary -->



<?php get_footer(); ?>