<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php include_once("analyticstracking.php") ?>

<section id="page" class="page-container-site">

	<header id="masthead" class="site-header" role="banner">

	<!-- <button id="menu-toggle" class="menu-toggle"><?php _e( 'Menu', 'twentysixteen' ); ?></button> -->

			<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<div id="site-header-menu" class="site-header-menu clearfix">
				<nav id="site-navigation" class="main-navigation site-inner" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						 ) );
					?>
				</nav><!-- .main-navigation -->
			</div><!-- .site-header-menu -->
			<?php endif; ?>


			<div id="site-header-sticky" class="site-header-sticky clearfix">

				<div class="site-inner">

					<?php dynamic_sidebar( 'sidebar-tel' ); ?>

					<?php if ( has_nav_menu( 'social' ) ) : ?>
					<nav id="sticky-navigation" class="sticky-navigation" role="navigation">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'sticky-menu',
								'depth'          => 1,
								'link_before'    => '<span class="sticky-link">',
								'link_after'     => '</span>',
							) );
						?>
					</nav><!-- .social-navigation -->
					<?php endif; ?>

				</div>

			</div><!-- .site-header-menu-top -->


			<div class="site-header-branding site-inner clearfix">
				<div class="site-branding">

						<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>


						<?php if ( is_page_template( 'contenu' ) ) {

							$description = get_bloginfo( 'description', 'display' );

							echo '<p class="site-description">' . $description . '</p>';
						}

						?>

						<?php if ( is_page_template( 'player' ) ) {

							$description = get_bloginfo( 'description', 'display' );

							echo '<p class="site-description">' . $description . '</p>';
						}

						?>
				</div>

			</div><!-- .site-header-main -->

	</header><!--.siter-header-->

	<main id="main" class="site-main clearfix" role="main">

		<section id="page-content" class="site-inner">

			<div class="container-since"><span class="content-since">Depuis 1995</span></div>
			<?php

				if ( is_front_page() ){

					$description = get_bloginfo( 'description', 'display' );

					echo '<h1 class="title-page site-content">' . $description . '</h1>';

				}else{

					echo '<h1 class="title-page title-page-contenu site-content">' . get_the_title() . '</h1>';

				}
			?>