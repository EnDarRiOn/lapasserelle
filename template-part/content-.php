<article class="widget-client">
	<?php
		$clients = get_field( 'logos_clients' );
		$size = 'full';

		if( $clients ):
	?>
		<div id="carrousel" class="carrousel-container cleafix">

			<ul class="list-clients carrousel">

				<?php foreach ($clients as $client ):?>

				<li class="item-list-client">
					<?php echo wp_get_attachment_image( $client['ID'], $size ); ?>
				</li>

				<?php endforeach; ?>
			</ul>

		<?php endif; ?>
		</div>

		<h2 class="widget-block wigdet-block-client">Ils nous font confiance</h2>

</article>