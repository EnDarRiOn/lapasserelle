<?php get_header();?>


<?php	if ( is_page( '129' ) ):?>

		<section id="primary" class="content-primary clearfix">

		<?php get_template_part( 'template-page/tpl-page', 'destination' );

	elseif (is_page( '289' ) ) :?>

		<section id="primary" class="content-primary clearfix">

		<?php get_template_part( 'template-page/tpl-page', 'emission' );


	elseif (is_page( '468' ) ) :?>

		<section id="primary" class="content-primary clearfix">

		<?php get_template_part( 'template-page/tpl-page', 'agenda' );

	else :?>

		<?php get_template_part( 'template-page/tpl-page', 'content' );

	endif;?>

<?php get_footer();?>