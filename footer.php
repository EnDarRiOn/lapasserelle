		</section><!-- .site-inner -->

	</main><!-- .site-main -->

	<footer id="section-map" class="site-footer" role="contentinfo">

		<?php echo do_shortcode( '[wpgmza id="2"]' ); ?>

		<?php if ( has_nav_menu( 'menu-copyright' ) ) : ?>

		<section class="footer-navigation site-inner">

			<nav class="copyright-navigation" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-copyright',
						'menu_class'     => 'copyright-links-menu',
						'depth'          => 1,
						'link_before'    => '<span class="link-copyright">',
						'link_after'     => '</span>',
					) );
				?>
			</nav><!-- .copyright-navigation -->

		</section>

		<?php endif; ?>

	</footer><!-- .site-footer -->

</section><!-- .page-container-site -->
<?php wp_footer(); ?>
</body>
</html>